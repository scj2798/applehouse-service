
import './App.css';
import {
  Route,
  Link,
  Routes
} from "react-router-dom";

//
import Login from './components/login';
import Post from './components/posts/post';
import MyPost from './components/posts/mypost';
import SignUp from './components/signup';

function App() {
  return (
    <Routes>
      <Route
          path="login"
          element={<Login />}
      />
      <Route
          path="signup"
          element={<SignUp />}
      /> 
      <Route
          path="post/*"
          element={<Post />}
      >
      </Route>

    </Routes>
   
    
  );
}

export default App;

import { Button, Container, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import axios from "axios";
import { useState } from 'react'
import { Link, useNavigate } from "react-router-dom";
import {useCookies} from "react-cookie";

enum Role {
    Admin = 2,
    User = 1,
}


const Login = () =>{

    const [email, setEmail] = useState('');
    const [psw, setPsw] = useState('');
    const [cookies, setCookie] = useCookies(['access_token', 'refresh_token','is_admin'])
    let navigate = useNavigate();

    const handleEmailTxt = (event:any) => {
        setEmail(event.target.value)
    };

    const handlePswTxt = (event:any) => {
        setPsw(event.target.value)
    };

    const onSubmit = async (e:any) => {
    try {
       e.preventDefault();
       console.log(email);
       console.log(psw);

      const res = await axios.post("http://localhost:4000/login", { email,psw });
      if(res.status != 200) throw new Error("Invalid Login");
      
        let expires = new Date()
        expires.setTime(expires.getTime() + (res.data.expires_in * 1000))
        setCookie('access_token', res.data.access_token, { path: '/',  expires})
        setCookie('refresh_token', res.data.refresh_token, {path: '/', expires})
        setCookie('is_admin', res.data['user_data']['role'] == Role.Admin, {path: '/', expires})
    
      navigate("/post",{ state: { token: res.data['access_token'], isAdmin:res.data['user_data']['role'] == Role.Admin }});

    } catch (error) {
       console.log(error);
       navigate("/login");
    }

    }

    return (
        <Container maxWidth="xl">
            <Box component="form" sx={{'& > :not(style)': { m: 4, width: '100ch',alignItems:'center', },}} noValidate autoComplete="off">
                <div>
                <Box sx={{ display: 'flex', alignItems: 'center', textAlign: 'center' }}>
                    <Typography variant="h4" color="text.secondary" > Login  </Typography>
                </Box>  
            
                </div>
                <div>
                    <TextField id="outlined-basic" label="Email" variant="outlined" value={email} onChange={handleEmailTxt} />
                </div>
                <div>
                    <TextField id="outlined-basic" label="Password" variant="outlined" value={psw} onChange={handlePswTxt} type="password" />
                </div>
                <div>
                    <Button variant="contained" color="primary" onClick={onSubmit}>Login</Button>
                </div>
                <div>
                    <Link  to={{pathname:"/signup"}} >
                        <Typography sx={{ minWidth: 100 }}>Register as new user</Typography>
                    </Link>
                </div>
               

            </Box>
        </Container>
    );
}

export default Login;
import { Button,Container,Typography } from "@mui/material";
import Box from "@mui/material/Box";
import axios from "axios";
import { useState } from 'react'
import { useLocation, useNavigate } from "react-router-dom";
import {useCookies} from "react-cookie";
import TextField from "@mui/material/TextField";

const AddPost = () =>{

   
    let navigate = useNavigate();
    const {state} = useLocation();
    const [cookies, setCookie, removeCookie] = useCookies(['access_token']);

    const [content,setContent] = useState([]);

    const onSubmit = async () => {
        try {
            console.log(state);

            const config = {
                headers: { Authorization: `Bearer ${cookies.access_token}` }
            }
            
            const res = await axios.post("http://localhost:4000/post",{content},config);
            if(res.status != 201) throw new Error("Something went wrong");
            navigate("/post/my");
            
          } catch (error) {
             console.log(error);
             navigate("/post/my");
          }
    }

    return (
        <Container maxWidth="xl">
            <Box component="form" sx={{'& > :not(style)': { m: 4, width: '100ch',alignItems:'start', },}} noValidate autoComplete="off">
                <div>
                    <Typography variant="h4" color="text.secondary" > Add New Post  </Typography> 
                </div>
                <div>
                    <TextField id="outlined-basic" label="Content" variant="outlined" multiline
                         minRows={4} maxRows={6} value={content} onChange={(e:any) => setContent(e.target.value)} />
                </div>
                <div>
                    <Button variant="contained" color="primary" onClick={onSubmit}>Submit</Button>
                </div>
            </Box>
          
        </Container>
    );
}


export default AddPost;
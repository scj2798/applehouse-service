import { Avatar, Button, Card, CardActions, CardContent, CardHeader, Chip, Container,Typography } from "@mui/material";
import Box from "@mui/material/Box";
import { red } from "@mui/material/colors";
import axios from "axios";
import { useEffect, useState } from 'react'
import { useLocation, useNavigate } from "react-router-dom";
import {useCookies} from "react-cookie";

enum Estatus{
    Pending = 0,
    Active = 1,
    Reject = 2
}

interface Iuser{
    id:number,
    name:string,
    email:string,
}

interface Ipost{
  id:number,
  content:string,
  User:Iuser
  status:Estatus
}

function getStatus(status:Estatus):any{
    switch (status) {
        case Estatus.Active:
            return <Chip label="Active" color={"success"} />;
        case Estatus.Reject:
            return <Chip label="Rejected" color={"error"} />;
        default:
            return <Chip label="Pending" color={"primary"} />;
    }
}

const RejectPost = () =>{

    const [post,setPost] = useState([]);
    let navigate = useNavigate();
    const {state} = useLocation();
    const [cookies, setCookie, removeCookie] = useCookies(['access_token']);

    useEffect(() => {
        const getData = async () => {
            try {
                console.log(state);

                const config = {
                    headers: { Authorization: `Bearer ${cookies.access_token}` }
                }
                
                const res = await axios.get("http://localhost:4000/post/pending",config);
                setPost(res.data['data'].map((d:Ipost) => d ));
          
              } catch (error) {
                 console.log(error);
                 navigate("/post");
              }
        };
        getData();
        
     },[]);

    const onApprove = async (id:number) => {
        try {
            const config = {
                headers: { Authorization: `Bearer ${cookies.access_token}` }
            }
            
            const res = await axios.put(`http://localhost:4000/post/approve/${id}`,{},config);
            if(res.status != 200) throw new Error("Something went wrong");
            window.location.reload();
      
          } catch (error) {
             console.log(error);
             window.location.reload();
          } 
    }

    const onReject = async (id:number) => {
        try {
            const config = {
                headers: { Authorization: `Bearer ${cookies.access_token}` }
            }
            
            const res = await axios.put(`http://localhost:4000/post/reject/${id}`,{},config);
            if(res.status != 200) throw new Error("Something went wrong");
            window.location.reload();
      
          } catch (error) {
             console.log(error);
             window.location.reload();
          } 
    }

    return (
        <Container maxWidth="xl">

            <Box sx={{ display: 'flex', alignItems: 'center', textAlign: 'center' }}>
                <Typography variant="h4" color="text.secondary" sx={{marginLeft:8, }}> Pending </Typography>
            </Box>

            <div>
            {
                post.map((p:Ipost,index:number) => (
                <Card key={`PCARD${index}`} sx={{ m:8, maxWidth: '80ch' } }>
                <CardHeader
                    avatar={
                    <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
                        {p.User.name[0]}
                    </Avatar>
                    }
                    action={getStatus(p.status)}
                    title={p.User.name}   
                />
                
                <CardContent>
                    <Typography variant="body2" color="text.secondary">{p.content}
                    </Typography>
                </CardContent>

                <CardActions disableSpacing>
                    <Button sx={{m:2}} variant="contained" color="success"  onClick={() => onApprove(p.id)} > Approve</Button>
                    <Button sx={{m:2}} variant="outlined" color="error"  onClick={() => onReject(p.id)} > Reject</Button>
                </CardActions>
                
                </Card> 
                ))
            }
            </div>
            
        </Container>
    );
}

export default RejectPost;
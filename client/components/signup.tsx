import { Button, Container, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import axios from "axios";
import { useState } from 'react'
import { useNavigate } from "react-router-dom";
import {useCookies} from "react-cookie";

enum Role {
    Admin = 2,
    User = 1,
}


const SignUp = () =>{

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [psw, setPsw] = useState('');

    const [cookies, setCookie] = useCookies(['access_token', 'refresh_token','is_admin'])
    let navigate = useNavigate();


    const onSubmit = async (e:any) => {
    try {

       e.preventDefault();
 
      const res = await axios.post("http://localhost:4000/signup", { name,email,psw });
      if(res.status != 200) throw new Error("Invalid Login");
      
        let expires = new Date()
        expires.setTime(expires.getTime() + (res.data.expires_in * 1000))
        setCookie('access_token', res.data.data.token.access_token, { path: '/',  expires})
        setCookie('refresh_token', res.data.data.token.refresh_token, {path: '/', expires})
        
        navigate("/post");

    } catch (error) {
       console.log(error);
       navigate("/login");
    }

    }

    return (
        <Container maxWidth="xl">
            <Box component="form" sx={{'& > :not(style)': { m: 4, width: '100ch',alignItems:'center', },}} noValidate autoComplete="off">
                <div>
                    
                <Box sx={{ display: 'flex', alignItems: 'center', textAlign: 'center' }}>
                    <Typography variant="h4" color="text.secondary" > Sign Up  </Typography>
                </Box>  
            
                </div>
                <div>
                    <TextField id="outlined-basic" label="Name" variant="outlined" value={name} onChange={(e) => setName(e.target.value)} />
                </div>
                <div>
                    <TextField id="outlined-basic" label="Email" variant="outlined" value={email} onChange={(e) => setEmail(e.target.value)} />
                </div>
                <div>
                    <TextField id="outlined-basic" label="Password" variant="outlined" value={psw} onChange={(e) => setPsw(e.target.value)} type="password" />
                </div>
                <div>
                    <Button variant="contained" color="primary" onClick={onSubmit}>Login</Button>
                </div>
            </Box>
        </Container>
    );
}

export default SignUp;
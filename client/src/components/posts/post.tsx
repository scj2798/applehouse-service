import { Avatar, Button, Card, CardActions, CardContent, CardHeader, Chip, Container, IconButton, Typography } from "@mui/material";
import Box from "@mui/material/Box";
import { red } from "@mui/material/colors";
import Icons from '@mui/material/Icon';

import axios from "axios";
import { useEffect, useState } from 'react'
import { Link, Navigate, Route, Routes, useLocation, useNavigate } from "react-router-dom";
import {useCookies} from "react-cookie";
import MyPost from "./mypost";
import PendingPost from "./pendingpost";


enum Estatus{
    Pending = 0,
    Active = 1,
    Reject = 2
}

interface Iuser{
    id:number,
    name:string,
    email:string,
}

interface Ipost{
  id:number,
  content:string,
  User:Iuser
  status:Estatus
}


function getStatus(status:Estatus):any{
    switch (status) {
        case Estatus.Active:
            return <Chip label="Active" color={"success"} />;
        case Estatus.Reject:
            return <Chip label="Reject" color={"error"} />;
        default:
            return <Chip label="Pending" color={"primary"} />;
    }
}

const AllPost = () =>{

    const [post,setPost] = useState([]);
    let navigate = useNavigate();
    
    const [cookies, setCookie, removeCookie] = useCookies(['access_token']);

    useEffect(() => {
        const getData = async () => {
            try {

                const config = {
                    headers: { Authorization: `Bearer ${cookies.access_token}` }
                }
                
                const res = await axios.get("http://localhost:4000/post/active",config);
                setPost(res.data['data'].map((d:Ipost) => d ));
          
              } catch (error) {
                 console.log(error);
                 navigate("/login");
              }
        };
        getData();
        
     },[]);

    return (
        <Container maxWidth="xl">

            <Box sx={{ display: 'flex', alignItems: 'center', textAlign: 'center' }}>
                <Typography variant="h4" color="text.secondary" sx={{marginLeft:8, }}> All  </Typography>
            </Box>  
            
            { post.length == 0 && <Box sx={{ display: 'flex', alignItems: 'center', textAlign: 'center' }}>
                <Typography variant="body1" color="text.secondary" sx={{m:8, }}> No posts  </Typography>
            </Box> }

            <div>
            {
                post.map((p:Ipost,index:number) => (
                <Card key={`PCARD${index}`} sx={{ m:8, maxWidth: '80ch' } }>
                <CardHeader
                    avatar={
                    <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">
                        {p.User.name[0]}
                    </Avatar>
                    }
                    action={
                        getStatus(p.status)
                      }
                    title={p.User.name}
                   
                    
                />
                
                <CardContent>
                    <Typography variant="body2" color="text.secondary">{p.content}
                    </Typography>
                </CardContent>
                
                </Card> 
                ))
            
            }
            </div>
            
        </Container>
    );
}

const Post = ()=>{

    const [cookies, setCookie, removeCookie] = useCookies(['is_admin']);
    const [isAdmin,setIsAdmin] = useState(cookies.is_admin);
    
    useEffect(() => {
        console.log(isAdmin);
     },[setIsAdmin]);

    return (
        <Container maxWidth="xl">

            <Box sx={{ display: 'flex', alignItems: 'center', textAlign: 'center' }}>
                <Typography variant="h3" color="text.secondary" sx={{m:8, }}> Posts </Typography>

                <Link  to={{pathname:""}} >
                    <Typography sx={{ minWidth: 100 }}>All Posts</Typography>
                 </Link>
                
                <Link  to={{pathname:"/post/my"}} >
                    <Typography sx={{ minWidth: 100 }}>My Posts</Typography>
                 </Link>

                 {/* <Link  to={{pathname:"/post/reject"}} >
                        <Typography sx={{ minWidth: 100 }}>Rejected Posts</Typography>
                        </Link> */}

                { isAdmin && <Link  to={{pathname:"/post/pending"}} >
                        <Typography sx={{ minWidth: 100 }}>Pending Approval</Typography>
                        </Link>
                    }

                 

               
            </Box>

            <div>
                <Routes>
                    <Route path="my/*" element={<MyPost/>}/>
                    <Route path="pending" element={<PendingPost/>}/>
                    <Route path="reject" element={<PendingPost/>}/>
                    <Route path="" element={<AllPost/>}/>
                </Routes>
            </div>
     
        </Container>
    );
}



export default Post;
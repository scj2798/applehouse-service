
const AuthService = require('../service/authService')

// Sign Up as guest
module.exports.guestSignUp = async(req,res,next) =>{
    try {
        const {name,email,psw} = req.body;
        const result = await AuthService.signUp({name,email,psw,role:1});
        res.status(200).json(result);

    } catch (error) {
        next(error);
    }
}


// Sign Up as Admin
module.exports.AdminSignUp = async(req,res,next) =>{
    try {
        const {name,email,psw} = req.body;
        const result = await AuthService.signUp({name,email,psw,role:2});
        res.status(200).json(result);
    } catch (error) {
        next(error);
    }
}

// Login
module.exports.login = async(req,res,next) =>{
    try {
        const {email,psw} = req.body;
        const result = await AuthService.login({email,psw});
        res.status(200).json(result);

    } catch (error) {
        next(error);
    }
}


const { Comment,User,Post } = require('../models')

// Create comments 
module.exports.createComment = async (req,res,next) =>{
  try {
    const {comment,userid,postid} = req.body;
    const result = await Comment.create({comment,userid,postid});
    res.status(201).json({msg:"ADDED COMMENT",data:result});
  } catch (error) {
    next(error);
  }
}

// Get All Comments
module.exports.getAllComments = async (req,res,next) =>{
  try {
    const result = await Comment.findAll({
      include:[{
        model:User,
        attributes:['name','email','id']
      },{
        model:Post,
      }]
    
    });
    res.status(200).json({msg:"ADDED COMMENT",data:result});
  } catch (error) {
    next(error);
  }
}

// Get Comments details by Id
module.exports.getCommentById = async (req,res,next) =>{
  try {
    const id = req.params.id;
    const result = await Comment.findOne(
      {
        where:{id}
      }
    );
    res.status(200).json({msg:"ADDED COMMENT",data:result});
  } catch (error) {
    next(error);
  }
}

// Update Comment
module.exports.updateComment = async (req,res,next) =>{
  try {
    const id = req.params.id;
    const body = req.body;
    const result = await Comment.update(body,
      {
        where:{id}
      }
    );
    res.status(200).json({msg:"UPDATED COMMENT",data:result});
  } catch (error) {
    next(error);
  }
}

// Delete Comment
module.exports.deleteComment = async (req,res,next) =>{
  try {
    const id = req.params.id;
    const result = await Comment.destroy({
      where:{id}
    });
    res.status(200).json({msg:"DELETED COMMENT",data:result});
  } catch (error) {
    next(error);
  }
}




const {Feedback} = require('../models')

// Create Feedback
module.exports.createFeedBack = async (req,res,next) =>{
  try {
    const {comment,userid,postid} = req.body;
    const result = await Feedback.create({comment,userid,postid});
    res.status(201).json({msg:"ADDED COMMENT",data:result});
  } catch (error) {
    next(error);
  }
}

// Get All Feedbacks
module.exports.getAllFeedBack = async (req,res,next) =>{
  try {
    const result = await Feedback.findAll();
    res.status(200).json({msg:"ADDED COMMENT",data:result});
  } catch (error) {
    next(error);
  }
}

// Get All Feedbacks
module.exports.getFeedBackById = async (req,res,next) =>{
  try {
    const id = req.params.id;
    const result = await Feedback.findOne(
      {
        where:{id}
      }
    );
    res.status(200).json({msg:"ADDED FEEDBACK",data:result});
  } catch (error) {
    next(error);
  }
}

module.exports.updateFeedBack  = async (req,res,next) =>{
  try {
    const id = req.params.id;
    const body = req.body;
    const result = await Feedback.update(body,
      {
        where:{id}
      }
    );
    res.status(200).json({msg:"UPDATED FEEDBACK",data:result});
  } catch (error) {
    next(error);
  }
}

module.exports.deleteFeedBack  = async (req,res,next) =>{
  try {
    const id = req.params.id;
    const result = await Feedback.destroy({
      where:{id}
    });
    res.status(200).json({msg:"DELETED FEEDBACK",data:result});
  } catch (error) {
    next(error);
  }
}



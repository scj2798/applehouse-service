
const {Post,User,Feedback, sequelize} = require('../models')

module.exports.createPost = async (req,res,next) =>{
  try {
    
    const {content} = req.body;
    const userid = req.userId;

    const result = await Post.create({content,userid,status:0});
    res.status(201).json({msg:"ADDED COMMENT",data:result});
  } catch (error) {
    next(error);
  }
}

module.exports.getAllPost = async (req,res,next) =>{
  try {
    const result = await Post.findAll({
      
      include:[
        {
          model:User,
          attributes:['name','email','id']
        }
      ]
    });
    res.status(200).json({data:result});
  } catch (error) {
    next(error);
  }
}

module.exports.getAllActivePost = async (req,res,next) =>{
  try {
    const result = await Post.findAll({
      where:{status:1},
      include:[
        {
          model:User,
          attributes:['name','email','id']
        }
      ]
    });
    res.status(200).json({data:result});
  } catch (error) {
    next(error);
  }
}

module.exports.getAllPostByUserId = async (req,res,next) =>{
  try {
    const userId = req.userId;
    console.log(userId);

    const result = await Post.findAll({
      where:{userid:userId},
      include:[
        {
          model:User,
          attributes:['name','email','id']
        }
      ]
    });
    res.status(200).json({data:result});
  } catch (error) {
    next(error);
  }
}

module.exports.getAllPendingPost = async (req,res,next) =>{
  try {
    const result = await Post.findAll({
      where:{status:0},
      include:[
        {
          model:User,
          attributes:['name','email','id']
        }
      ]
    });
    res.status(200).json({data:result});
  } catch (error) {
    next(error);
  }
}

module.exports.toApprovePost = async (req,res,next) =>{
  try {
    const id = req.params.id;
    const result = await Post.update({status:1},{
      where:{id},
    });
    res.status(200).json({data:result});
  } catch (error) {
    next(error);
  }
}

module.exports.toRejectPost = async (req,res,next) =>{
  try {
    const postid = req.params.id;
    const {feedback} = req.body;
    const adminid = req.userId;

    await sequelize.transaction(async (t) => {

      await Feedback.create({feedback,adminid,postid},{ transaction: t });
      
      await Post.update({status:2},{
        where:{id:postid},
        transaction: t
      });

      res.status(200).json();
      
    });
    
  } catch (error) {
    next(error);
  }
}

module.exports.getPostById = async (req,res,next) =>{
  try {
    const id = req.params.id;
    const result = await Post.findOne(
      {
        where:{id}
      }
    );
    res.status(200).json({msg:"ADDED FEEDBACK",data:result});
  } catch (error) {
    next(error);
  }
}

module.exports.updatePost  = async (req,res,next) =>{
  try {
    const id = req.params.id;
    const body = req.body;
    const result = await Post.update(body,
      {
        where:{id}
      }
    );
    res.status(200).json({msg:"UPDATED FEEDBACK",data:result});
  } catch (error) {
    next(error);
  }
}

module.exports.deletePost  = async (req,res,next) =>{
  try {
    const id = req.params.id;
    const result = await Post.destroy({
      where:{id}
    });
    res.status(200).json({msg:"DELETED FEEDBACK",data:result});
  } catch (error) {
    next(error);
  }
}



const { Router } = require('express');
const route = Router();

route.route('/login')
    .get(async (req, res, next) => {
        try {
            const users = await UserService.findAllUsers();
            res.status(200).json({ msg: "OK", data: users });
        } catch (error) {
            next(error)
        }
});


module.exports = route;
module.exports = class HttpError extends Error {
    constructor(code,msg){
        super(code,msg);
        if(Error.captureStackTrace) Error.captureStackTrace(this,HttpError);
        this.name = "HttpError"
        this.code = code;
        this.message = msg;
    }
}
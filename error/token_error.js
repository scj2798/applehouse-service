module.exports = class TokenError extends Error {
    constructor(code,msg,status){
        super(code,msg);
        if(Error.captureStackTrace) Error.captureStackTrace(this,TokenError);
        this.name = "TokenError"
        this.code = code;
        this.message = msg;
        this.status = status;
    }
}
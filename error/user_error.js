module.exports = class UserError extends Error {
    constructor(code,msg){
        super(code,msg);
        if(Error.captureStackTrace) Error.captureStackTrace(this,UserError);
        this.name = "UserError"
        this.code = code;
        this.message = msg;
    }
}









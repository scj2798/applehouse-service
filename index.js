const express = require("express")
const bodyParser = require('body-parser')
const cors = require('cors');

//
const Routes = require('./routers')
const { sequelize } = require('./models')

//
const PORT = process.env.PORT || 4000

const app = express();

app.use(express.json())
app.use(bodyParser.json())
app.use(cors());
app.use(Routes)

app.listen(PORT, (err) => {
    try {
      console.log(`SERVER RUNNING ON ${PORT}`)
      sequelize.authenticate({
        force: true,
      })
      console.log('SEQUELIZE AUTHENTICATE')
      sequelize.sync()
      console.log('SEQUELIZE SYNC')
    } catch (e) {
      
      console.log('SERVER ERROR', e)
    }
})
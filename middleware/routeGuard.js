const jwt = require('jsonwebtoken')
const UserError = require('../error/user_error')
// require('dotenv').config()

module.exports.AdminGuard = (req, res, next) => {
  try {
    const { authorization } = req.headers

    if (!authorization && req.cookies.access_token == null)
      throw new UserError(403, 'UN AUTHORIZED')

    var key =
      authorization == null
        ? req.cookies.access_token
        : authorization.toString().split(' ')[1]

    jwt.verify(key, `${process.env.KEY}`, (err, payload) => {
      if (err) throw new UserError(403, err.message)
      else if (payload.role != 2) {
        throw new UserError(403, "YOU AREN'T ADMIN ")
      } else {
        req.userId = payload.user_id;
        next()
      }
    })
    
  } catch (error) {
    
    next(error)
  }
}

module.exports.UserGuard = async (req, res, next) => {
  try {
    const { authorization } = req.headers

    if (!authorization && req.cookies.access_token == null)
      throw new UserError(400, 'BAD REQUEST')

    var key =
      authorization == null
        ? req.cookies.access_token
        : authorization.toString().split(' ')[1]

    jwt.verify(key, `${process.env.KEY}`, (err, payload) => {
      console.log(err)
      console.log(payload)
      if (err) throw new UserError(403, err.message)
      else {
        console.log(payload);
        req.userId = payload.user_id;
        next()
      }
    })
  } catch (error) {
    next(error)
  }
}
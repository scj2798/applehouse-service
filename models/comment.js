'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Comment extends Model {
    
    static associate(models) {
      
      // Associate with User ( 1 : M )
      this.belongsTo(models.User,{
        foreignKey:'userid', 
      });

      // Associate with Post ( 1 : M )
      this.belongsTo(models.Post,{
        foreignKey:'postid',
      });



    }
  }
  Comment.init({
    comment: DataTypes.STRING,
    postid: DataTypes.INTEGER,
    userid: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Comment',
  });
  return Comment;
};
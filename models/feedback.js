'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Feedback extends Model {
    
    static associate(models) {
      
      // Associate with user ( 1 : 1 )
      this.belongsTo(models.User,{
        foreignKey:'adminid'
      });

      // Associate with Post ( 1 : 1 )
      this.belongsTo(models.Post,{
        foreignKey:'postid',
      });

    }
  }
  Feedback.init({
    feedback: DataTypes.STRING,
    postid: DataTypes.INTEGER,
    adminid: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Feedback',
  });
  return Feedback;
};
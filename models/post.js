'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Post extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {

      // Associate with Comment ( 1 : M )
      this.hasMany(models.Comment,{
        foreignKey:'postid',
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      })

      // Associate with User ( 1 : M )
      this.belongsTo(models.User,{
        foreignKey:'userid'
      });

       // Associate with Feedback ( 1 : 1 )
       this.hasOne(models.Feedback,{
        foreignKey:'postid',
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        
      })

    }
  }
  Post.init({
    content: DataTypes.STRING,
    userid: DataTypes.INTEGER,
    status: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Post',
  });
  return Post;
};
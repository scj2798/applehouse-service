'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
   
    static associate(models) {

      // Associate with Comment ( 1 : M )
      this.hasMany(models.Comment,{
        foreignKey:'userid',
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      })

       // Associate with Post ( 1 : M )
       this.hasMany(models.Post,{
        foreignKey:'postid',
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      })

      // Associate with Feedback ( 1 : 1 )
      this.hasOne(models.Feedback,{
        foreignKey:'adminid',
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      })
      
    }
  }
  User.init({
    name: DataTypes.STRING,
    email: {
      unique:true,
      type:DataTypes.STRING,
      allowNull:false
    },
    psw: {
      type:DataTypes.STRING,
      allowNull:false
    },
    role: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};
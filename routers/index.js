const { Router } = require('express');
const swaggerUi = require('swagger-ui-express')
const Joi = require('joi')
const { AdminGuard, UserGuard } = require('../middleware/routeGuard');
const { Sequelize } = require('../models');

// Error 
const UserError = require('../error/user_error');
const TokenError = require('../error/token_error');
const HttpError = require('../error/http_error');

// Controllers
const AuthController = require('../controller/authController');
const CommentController = require('../controller/commentController');
const FeedbackController = require('../controller/feedbackController');
const PostController = require('../controller/postController');

//
const app = Router();

/**
 *  Routing 
 * 
 */

//Sign Up
app.post('/signup',AuthController.guestSignUp);
app.post('/signup/admin',AdminGuard,AuthController.AdminSignUp);

// Login
app.post('/login',AuthController.login);

app.post('/comment',UserGuard,CommentController.createComment);
app.get('/comment',UserGuard,CommentController.getAllComments);
app.get('/comment/:id',UserGuard,CommentController.getCommentById);
app.put('/comment/:id',UserGuard,CommentController.updateComment);
app.delete('/comment/:id',UserGuard,CommentController.deleteComment);

// Feedback
app.post('/feedback',UserGuard,FeedbackController.createFeedBack);
app.get('/feedback',UserGuard,FeedbackController.getAllFeedBack);
app.get('/feedback/:id',UserGuard,FeedbackController.getFeedBackById);
app.put('/feedback/:id',UserGuard,FeedbackController.updateFeedBack);
app.delete('/feedback/:id',UserGuard,FeedbackController.deleteFeedBack);

// post
app.post('/post',UserGuard,PostController.createPost);
app.get('/post',UserGuard,PostController.getAllPost);
//
app.get('/post/active',UserGuard,PostController.getAllActivePost);
app.get('/post/pending',AdminGuard,PostController.getAllPendingPost);

app.put('/post/reject/:id',AdminGuard,PostController.toRejectPost);
app.put('/post/approve/:id',AdminGuard,PostController.toApprovePost);
//
app.get('/post/user',UserGuard,PostController.getAllPostByUserId);
app.get('/post/:id',UserGuard,PostController.getPostById);

app.put('/post/:id',UserGuard,PostController.updatePost);
app.delete('/post/:id',UserGuard,PostController.deletePost);

// Error Detection
app.use((err, req, res, next) => {
 
    if (err instanceof HttpError) {
      console.log(err);
      res.status(err.code).json({ error: err.name, msg: err.message });
      return;
    }
 
    else if (err instanceof UserError) {
     console.log(err);
     res.status(err.code).json({ error: err.name, msg: err.message });
     return;
     }
 
    else if (err instanceof TokenError) {
      console.log(err);
      res.status(err.code).json({ error: err.name, msg: err.message, status: err.status });
      return;
    }
    else if (err instanceof Joi.ValidationError) {
      console.log(err);
      res.status(400).json({ error: err.name, msg: err.message });
      return;
    }
    else if (err instanceof Sequelize.UniqueConstraintError) {
      console.log(err);
      var msgs = "";
      err.errors.forEach(e => {
        msgs = msgs.concat(e.message, ',')
      });
      res.status(409).json({ error: err.name, msg: msgs });
      return;
  
    }
  
    console.error("ERROR", err);
    res.status(500).json({ err });
    
});


module.exports = app;

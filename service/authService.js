require('dotenv').config();

const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const { User,sequelize } = require('../models');
const UserError = require('../error/user_error');
const TokenService = require('./tokenService');
const UserService = require('./userService');

//Login
module.exports.login = (credential) => {

    return new Promise(async (resolve, reject) => {
        
        try {

            const { email, psw } = credential;

            // Check user has found or Not using the given email
            const user = await User.findOne({
                where: {email},
            });

            if (!user) throw new UserError(404, "User Not Found");

            // check the given password is corrected Or not
            else if (!await bcrypt.compare(psw, user.psw)) throw new UserError(401, "Password Incorrect");

            var payload = {
                user_id:user.id,
                name:user.name,
                role:user.role,
                email:user.email
            }

            var {access_token,refresh_token} = await TokenService.generateTokensInLogin(payload);

            resolve({access_token,refresh_token,user_data:payload});

        } catch (error) {
            reject(error);
        }

    });
}

// Sign Up
module.exports.signUp =  (userData) => {

        return sequelize.transaction(async (t) => {
            
            var {name,email,psw,role} = userData;

            //Check password is null Or not, if Password is null , throw an error.
            if(!psw || psw == null) throw new UserError(400,"Password not found");
            
            var saltRound = 10;
            psw = await bcrypt.hash(psw, saltRound);
            
            // Add User details
            const user = await UserService.addUser({name,email,psw,role},t);
            const token = await TokenService.generateTokens({ user_id: user.id, role: user.role },t);

            return ({ msg: "USER CREATED", data: { token, user_data:user } });

        });
 };

module.exports.logout = () => {
    
    return new Promise(async (resolve, reject) => {
        try {
            var token = jwt.sign({}, `${process.env.KEY}`, {
                expiresIn: (0)
            });
            resolve({token});

        } catch (error) {
            reject(error);
        }

    });
}






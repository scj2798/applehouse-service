const jwt = require('jsonwebtoken');
const UserError = require('../error/user_error');
const { Token } = require('../models');
const UserService = require('./userService');

module.exports.generateTokens = ({ user_id, role},t) => {

    return new Promise(async (resolve, reject) => {
        try {

            // Generate Refresh Token
            var refresh_token = jwt.sign({ user_id }, `${process.env.KEY}`, {
                expiresIn: (3600 * 24)
            });


            // Store Refresh Token
            var rToken = await Token.create({ user_id, refresh_token },{ transaction: t });
            console.log("Store refresh token");

            var accessToken_payload = {
                role,
                user_id,
            }

            //Generate Access Token
            var access_token = jwt.sign(accessToken_payload, `${process.env.KEY}`, {
                expiresIn: (3600)
            });

            resolve({access_token,refresh_token});

        } catch (error) {
            reject(error);
        }

    });
}

module.exports.generateTokensInLogin = ({ user_id, role }) => {

    return new Promise(async (resolve, reject) => {
        try {

            // Generate Refresh Token
            var refresh_token = jwt.sign({ user_id }, `${process.env.KEY}`, {
                expiresIn: (3600 * 24)
            });


            // update Refresh Token
            var rToken = await Token.update({ refresh_token }, { where: { user_id } });
            console.log("Store refresh token");

            var accessToken_payload = {
                role,
                user_id
            }

            //Generate Access Token
            var access_token = jwt.sign(accessToken_payload, `${process.env.KEY}`, {
                expiresIn: (3600)
            });

            resolve({access_token,refresh_token});

        } catch (error) {
            reject(error);
        }

    });
}

module.exports.generateAccessTokenByUserId = (user_id) => {

    return new Promise(async (resolve, reject) => {
        try {
            var result = await Token.findOne({
                where: { user_id }
            });
          
            if (result === null) throw new UserError(404, "doesn't have any refresh token related user");

            // check refresh token is valid nor
            jwt.verify(result.refresh_token, `${process.env.KEY}`, async (err, payload) => {
                if (err) throw new UserError(403, err.message);
                else {

                    var user = await UserService.findUserById(payload.user_id);
                    
                    var accessToken_payload = {
                        role_id: user.role,
                        user_id: user.id,
                    }

                    //Generate Access Token
                    var accessToken = jwt.sign(accessToken_payload, `${process.env.KEY}`, {
                        expiresIn: (3600)
                    });

                    resolve({ accessToken });
                }
            });

        } catch (error) {
            reject(error);
        }

    });
}

module.exports.storeRefreshToken = (user_id, refresh_token) => {
    return Token.create({ user_id, refresh_token });
}

module.exports.getRefreshTokenByUserId = (user_id) => {
    return Token.findOne({ where: { user_id } });
}

module.exports.updateRefreshTokenByUserId = ({ user_id, refresh_token }) => {

    return Token.update({ refresh_token }, {
        where: { user_id }
    });
}

module.exports.deleteRefreshTokenByUserId = (user_id) => {

    return new Promise(async (resolve, reject) => {
        try {
            var token = await Token.destory({
                where: { user_id }
            });
            resolve(token.refresh_token);
        } catch (error) {
            reject(error);
        }

    });
}


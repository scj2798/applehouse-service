
const UserError = require('../error/user_error');
const { User } = require('../models');

var  retrieveDataFormat = [
    "id",
    "name",
    "email",
    "createdAt",
    "updatedAt"
];

exports.addUser = (user,t) => {
    return User.create(user,{ transaction: t });
}

exports.findAllUsers = () => {
    return User.findAll({
       attributes:retrieveDataFormat
    });
}

exports.findUserByEmail = (email) => {
    return new Promise(async (reslove,reject) => {
        try{
            const users = await User.findOne({
                where:{email},
                attributes:retrieveDataFormat
            });
            reslove(users);
        }catch(e){
            reject(e);
        }
    })
}

exports.findUserById = (uuid) => {
    return new Promise(async (resolve,reject) => {
        try {
            const user = await User.findOne({ where:{uuid},attributes:retrieveDataFormat});
            if(!user) throw new UserError(404,"User not found");
            resolve(user);
        } catch (error) {
            reject(error);
        }
    });
}

exports.updateUser = (uuid,data) => {
    return new Promise(async (resolve,reject) => {
        try {
            const user = await User.findOne({ where:{uuid},attributes:retrieveDataFormat});
            if(!user) throw new UserError(404,"User not found");
            await User.update(data,{where:{uuid}});
            resolve();
        } catch (error) {
            reject(error);
        }
    });    
}

exports.deleteUser = (uuid) => {
    return new Promise(async (reslove,reject) => {
        try{
            const user = await User.findOne({
                where:{uuid}
            });
            if(!user) throw new UserError(404,"User not found");
           
            reslove();
        }catch(e){
            reject(e);
        }
    })
}



